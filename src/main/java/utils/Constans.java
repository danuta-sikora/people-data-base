package utils;

public class Constans {
    public static final String ENTER_ID_MESSAGE = "Please enter ID:";
    public static final String NO_PERSON_IN_DB_INFO = "No person in data base";
    public static final String NO_PERSON_IN_DB_WITH_ID = "No person in data base with ID: ";
    public static final String NAME = "Name:";
    public static final String SURNAME = "Surname:";
    public static final String AGE = "Age:";
    public static final String HEIGHT = "Height (in CM):";
    public static final String WEIGHT = "Weight format (00.00):";
    public static final String CITY = "ADDRESS - City:";
    public static final String ZIP_CODE = "ADDRESS - Zip code:";
    public static final String STREET = "ADDRESS - Street:";
    public static final String HOME_NUMBER = "ADDRESS - Home number:";
    public static final String PERSON_REMOVED_INFO = "Person removed from database successful";
    public static final String PERSON_ADDED_INFO = "Person added to the base with ID: ";
    public static final String SEARCH_BY_NAME_SURNAME_INFO = "Searching by name or surname. Please enter person's name or surname";
    public static final String SEARCH_BY_GIVEN_CRITERIA_INFO = "Please enter your criteria name/surname/age/city/street):";
}
