package pl.sda.services;

import pl.sda.domain.Person;

import java.util.List;
import java.util.Optional;

public interface PersonService {

    List<Person> findAll();

    Optional<Person> findById(int id);

    void add(Person person);

    void edit(Person person);

    void editAddress(Person person);

    void remove(int id);

    List<Person> searchByNameOrSurname(String criteria);

    void sortByCriteria(String criteria);

}
