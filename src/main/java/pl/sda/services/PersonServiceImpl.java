package pl.sda.services;

import pl.sda.dao.impl.PersonDaoHibernateImpl;
import pl.sda.domain.Person;

import java.util.List;
import java.util.Optional;

public class PersonServiceImpl implements PersonService {
    private static PersonDaoHibernateImpl personDaoHibernate;

    public PersonServiceImpl() {
        personDaoHibernate = new PersonDaoHibernateImpl();

    }

    @Override
    public List<Person> findAll() {
        List<Person> people = personDaoHibernate.getInfo();
        return people;
    }

    @Override
    public Optional<Person> findById(int id) {
        Optional<Person> person = personDaoHibernate.checkDetails(id);
        return person;
    }

    @Override
    public void add(Person person) {
        personDaoHibernate.add(person);
        System.out.println(person.getId());
    }

    @Override
    public void edit(Person person) {
        personDaoHibernate.edit(person);
        System.out.println(person);
    }

    @Override
    public void editAddress(Person person) {
        personDaoHibernate.editAddress(person);
        personDaoHibernate.edit(person);

    }

    @Override
    public void remove(int id) {
        personDaoHibernate.remove(id);
    }

    @Override
    public List<Person> searchByNameOrSurname(String criteria) {
        List<Person> byNameOrSurname = personDaoHibernate.queryByNameOrSurname(criteria);
        if (byNameOrSurname.isEmpty()) {
            System.out.println("No results with name/surname: " + criteria);
        }
        return byNameOrSurname;
    }


    @Override
    public void sortByCriteria(String criteria) {

        personDaoHibernate.sortByGivenCriteria(criteria);
    }

}
