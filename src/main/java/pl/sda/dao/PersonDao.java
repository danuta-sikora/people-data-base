package pl.sda.dao;

import pl.sda.domain.Person;


import java.util.List;
import java.util.Optional;

public interface PersonDao {

    List<Person> getInfo();

    Optional<Person> checkDetails(int id);

    int add(Person person);

    int edit(Person person);

    void remove(int id);

    int editAddress(Person person);

    List<Person> queryByNameOrSurname(String name);

    List<Person> sortByGivenCriteria(String criteria);

}
