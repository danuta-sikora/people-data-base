package pl.sda.dao.impl;

import org.apache.commons.lang3.reflect.Typed;
import org.hibernate.Criteria;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import pl.sda.domain.Address;
import pl.sda.domain.Person;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.List;
import java.util.Optional;


public class PersonDaoHibernateImpl implements pl.sda.dao.PersonDao {
    private static final SessionFactory sessionFactory;

    static {
        StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure("hibernate.cfg.xml")
                .build();
        Metadata metadata = new MetadataSources(registry)
                .buildMetadata();
        sessionFactory = metadata.buildSessionFactory();
    }

    @Override
    public List<Person> getInfo() {
        Session session = sessionFactory.openSession();
        List<Person> personList = session.createQuery("FROM Person", Person.class).list();
        session.close();
        return personList;
    }

    @Override
    public Optional<Person> checkDetails(int id) {
        Session session = sessionFactory.openSession();
        Person person = session.get(Person.class, id);
        session.close();
        return Optional.ofNullable(person);
    }

    @Override
    public int add(Person person) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.save(person);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }
        session.close();
        return person.getId();
    }

    @Override
    public int edit(Person person) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            session.update(person);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }
        session.close();
        return person.getId();
    }

    @Override
    public void remove(int id) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Person person = session.get(Person.class, id);
            session.delete(person);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }
        session.close();
    }


    @Override
    public int editAddress(Person person) {
        Session session = sessionFactory.openSession();
        Transaction transaction = null;
        try {
            transaction = session.beginTransaction();
            Address address = person.getAddress();
            session.update(address);
            transaction.commit();
        } catch (RuntimeException e) {
            if (transaction != null && transaction.isActive()) {
                transaction.rollback();
            }
        }
        session.close();
        return person.getId();

    }

    @Override
    public List<Person> queryByNameOrSurname(String name) {
        Session session = sessionFactory.openSession();
        List<Person> person = session.createQuery("FROM Person p WHERE p.name=:name OR p.surname=:name")
                .setParameter("name", name).list();
        session.close();
        return person;
    }


    @Override
    public List<Person> sortByGivenCriteria(String criteria) {
        Session session = sessionFactory.openSession();
        CriteriaBuilder criteriaBuilder = session.getCriteriaBuilder();

        CriteriaQuery<Person> criteriaQuery = criteriaBuilder.createQuery(Person.class);
        Root<Person> root = criteriaQuery.from(Person.class);
        switch (criteria) {
            case "name":
                criteriaQuery.select(root).orderBy(criteriaBuilder.asc(root.get("name")));

                break;
            case "surname":
                criteriaQuery.select(root).orderBy(criteriaBuilder.asc(root.get("surname")));

                break;
            case "age":
                criteriaQuery.select(root).orderBy(criteriaBuilder.asc(root.get("age")));

                break;
            case "city":
                criteriaQuery.select(root).orderBy(criteriaBuilder.asc(root.get("address").get("city")));

                break;
            case "street":
                criteriaQuery.select(root).orderBy(criteriaBuilder.asc(root.get("address").get("street")));

                break;
            default:
                System.out.println("Please enter criteria to sort by: name, surname, age or address");
        }
        TypedQuery<Person> typedQuery = session.createQuery(criteriaQuery);
        List<Person> resultList = typedQuery.getResultList();
        for (Person p : resultList) {
            System.out.println(p);


        }
        return resultList;
    }
}





