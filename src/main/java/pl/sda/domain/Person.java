package pl.sda.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;


@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "person")
public class Person implements Comparable<Person> {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(name = "name", nullable = false)
    private String name;
    @Column(name = "surname",nullable = false)
    private String surname;
    @Column
    private int age;
    @Column
    private int height;
    @Column
    private double weight;
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name="street", column = @Column(name="street")),
            @AttributeOverride(name="city", column = @Column(name = "city")),
            @AttributeOverride(name="zipCode",column = @Column(name = "zipCode")),
            @AttributeOverride(name="homeNumber",column = @Column(name="homeNumber"))
    })
    private Address address;


    @Override
    public int compareTo(Person o) {
        int compare = surname.compareTo(o.surname);
        if (compare == 0) {
            compare = name.compareTo(o.name);
        }
        return compare;
    }

}
