import pl.sda.domain.Address;
import pl.sda.domain.Person;
import pl.sda.services.PersonServiceImpl;

import java.util.*;


import static java.lang.Double.parseDouble;
import static java.lang.Integer.*;
import static utils.Constans.*;

public class Application {

    private static PersonServiceImpl personService = new PersonServiceImpl();
    private static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {

        do {
            AppMenu.print();

            int option = checkInputIsInt(scanner);

            AppMenu.ofKey(option).ifPresent(item -> {
                switch (item) {
                    case PEOPLE_INFO_LIST:
                        peopleInfoList();
                        break;
                    case CHECK_DETAILS:
                        checkPersonDetails(scanner);
                        break;
                    case ADD_PERSON:
                        addPerson(scanner);
                        break;
                    case EDIT_PERSON:
                        editPerson(scanner);
                        break;
                    case REMOVE_PERSON:
                        deletePerson(scanner);
                        break;
                    case EDIT_ADDRESS:
                        editPersonsAddress(scanner);
                        break;
                    case SEARCH_PERSON:
                        searchByNameOrSurname(scanner);
                        break;
                    case SORTED_BY_CRITERIA:
                        sortByGivenCriteria(scanner);
                        break;
                    case EXIT:
                        System.exit(0);
                    default:
                        break;
                }
            });

        } while (true);

    }

    private static void peopleInfoList() {
        List<Person> persons = personService.findAll();
        for (Person p : persons) {
            System.out.println(p.toString());
        }
        if (persons.isEmpty()) {
            System.out.println(NO_PERSON_IN_DB_INFO);
        }
    }

    private static void checkPersonDetails(Scanner scanner) {
        System.out.println(ENTER_ID_MESSAGE);
        int id = checkInputIsInt(scanner);

        Optional<Person> person1 = personService.findById(id);
        if (person1.isPresent()) {
            System.out.println(person1);

        } else {
            System.out.println(NO_PERSON_IN_DB_WITH_ID + id);
        }

    }


    private static void addPerson(Scanner scanner) {

        System.out.println(NAME);
        String name = scanner.nextLine();

        System.out.println(SURNAME);
        String surname = scanner.nextLine();

        System.out.println(AGE);
        int age = checkInputIsInt(scanner);

        System.out.println(HEIGHT);

        int height = checkInputIsInt(scanner);

        System.out.println(WEIGHT);

        double weight = checkInputIsDouble(scanner);

        System.out.println(CITY);
        String city = scanner.nextLine();

        System.out.println(ZIP_CODE);
        String zipCode = scanner.nextLine();

        System.out.println(STREET);
        String street = scanner.nextLine();

        System.out.println(HOME_NUMBER);
        int homeNumber = checkInputIsInt(scanner);

        Person person = new Person();
        person.setName(name);
        person.setSurname(surname);
        person.setAge(age);
        person.setWeight(weight);
        person.setHeight(height);
        Address address = new Address(city, zipCode, street, homeNumber);

        person.setAddress(address);
        personService.add(person);
        System.out.println(PERSON_ADDED_INFO + person.getId());

    }

    private static double checkInputIsDouble(Scanner scanner) {
        while (!scanner.hasNext("\\d+(\\.\\d+)?")) {
            System.out.println("Please enter your weight in format 00.00");
            scanner.nextLine();
        }
        return parseDouble(scanner.nextLine());
    }

    private static int checkInputIsInt(Scanner scanner) {
        while (!scanner.hasNextInt()) {
            System.out.println("Please enter a number");
            scanner.nextLine();
        }
        return parseInt(scanner.nextLine());
    }

    private static void editPerson(Scanner scanner) {
        System.out.println(ENTER_ID_MESSAGE);
        int idEdit = checkInputIsInt(scanner);
        Optional<Person> person = personService.findById(idEdit);
        if (person.isPresent()) {
            Person person1 = person.get();
            System.out.println(NAME);
            String name = scanner.nextLine();

            System.out.println(SURNAME);
            String surname = scanner.nextLine();

            System.out.println(AGE);
            int age = checkInputIsInt(scanner);

            System.out.println(HEIGHT);

            int height = checkInputIsInt(scanner);

            System.out.println(WEIGHT);
            double weight = checkInputIsDouble(scanner);

            System.out.println(CITY);
            String city = scanner.nextLine();

            System.out.println(ZIP_CODE);
            String zipCode = scanner.nextLine();

            System.out.println(STREET);
            String street = scanner.nextLine();

            System.out.println(HOME_NUMBER);
            int homeNumber = checkInputIsInt(scanner);

            person1.setName(name);
            person1.setSurname(surname);
            person1.setAge(age);
            person1.setWeight(weight);
            person1.setHeight(height);

            Address address = new Address(city, zipCode, street, homeNumber);
            person1.setAddress(address);
            personService.edit(person1);
        } else {
            System.out.println(NO_PERSON_IN_DB_WITH_ID + idEdit);
        }

    }

    private static void deletePerson(Scanner scanner) {
        System.out.println(ENTER_ID_MESSAGE);
        int idToRemove = checkInputIsInt(scanner);
        Optional<Person> person1 = personService.findById(idToRemove);
        if (person1.isPresent()) {
            personService.remove(idToRemove);
            System.out.println(PERSON_REMOVED_INFO);
        } else {
            System.out.println(NO_PERSON_IN_DB_WITH_ID + idToRemove);
        }
    }

    private static void editPersonsAddress(Scanner scanner) {
        System.out.println(ENTER_ID_MESSAGE);
        int idEdit = checkInputIsInt(scanner);

        Optional<Person> foundPerson = personService.findById(idEdit);
        if (foundPerson.isPresent()) {
            Person person2 = foundPerson.get();

            System.out.println(CITY);
            String city = scanner.nextLine();

            System.out.println(ZIP_CODE);
            String zipCode = scanner.nextLine();

            System.out.println(STREET);
            String street = scanner.nextLine();

            System.out.println(HOME_NUMBER);
            int homeNumber = checkInputIsInt(scanner);


            Address address = new Address(city, zipCode, street, homeNumber);

            person2.setAddress(address);
            personService.editAddress(person2);
            System.out.println(person2);

        } else {
            System.out.println(NO_PERSON_IN_DB_WITH_ID + idEdit);
        }
    }

    private static void searchByNameOrSurname(Scanner scanner) {
        System.out.println(SEARCH_BY_NAME_SURNAME_INFO);
        String nameOrSurname = scanner.nextLine();
        List<Person> names = personService.searchByNameOrSurname(nameOrSurname);

        for (Person p : names) {
            System.out.println(p.toString());
        }
    }

    private static void sortByGivenCriteria(Scanner scanner) {
        System.out.println(SEARCH_BY_GIVEN_CRITERIA_INFO);
        String criteria = scanner.nextLine();
        if (criteria.equals("name") ||
                criteria.equals("surname") ||
                criteria.equals("age") ||
                criteria.equals("city") ||
                criteria.equals("street")) {
            personService.sortByCriteria(criteria);

        } else {
            System.out.println("Wrong criteria!");
        }


    }

}
