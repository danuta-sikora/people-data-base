import java.util.Optional;

public enum AppMenu {
    PEOPLE_INFO_LIST(1, "giving list basic people info"),
    CHECK_DETAILS(2, "checking details of given person"),
    ADD_PERSON(3, "adding new person"),
    EDIT_PERSON(4, "editing person"),
    REMOVE_PERSON(5, "deleting person"),
    EDIT_ADDRESS(6, "editing person's address data"),
    SEARCH_PERSON(7, "looking for a person by name or surname"),
    SORTED_BY_CRITERIA(8, "sorting by given criteria (name/surname/age/city/street)"),
    EXIT(0, "exit");


    public final int key;
    public final String message;

    AppMenu(int key, String message) {
        this.key = key;
        this.message = message;
    }

    public static void print() {
        for (AppMenu item : AppMenu.values()) {
            System.out.println(item.key + " " + item.message);
        }
    }

    public static Optional<AppMenu> ofKey(int key) {
        for (AppMenu item : AppMenu.values()) {
            if (key == item.key) {
                return Optional.ofNullable(item);
            }
        }
        return Optional.empty();
    }

}

