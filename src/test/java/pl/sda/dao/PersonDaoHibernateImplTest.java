package pl.sda.dao;


import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import pl.sda.configuration.JdbcConnectionManager;
import pl.sda.configuration.PropertyReader;
import pl.sda.configuration.TestUtil;

import pl.sda.dao.impl.PersonDaoHibernateImpl;
import pl.sda.domain.Address;
import pl.sda.domain.Person;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.Java6Assertions.assertThat;


class PersonDaoHibernateImplTest {
    private pl.sda.dao.PersonDao sut;
    private JdbcConnectionManager connectionManager;

    @BeforeEach
    void setUp() throws Exception {
        sut = new PersonDaoHibernateImpl();
        connectionManager = new JdbcConnectionManager(PropertyReader.loadConfiguration());
        TestUtil.setUpDatabase(connectionManager);
    }

    @AfterEach
    void tearDown() throws Exception {
        TestUtil.cleanUpDatabase(connectionManager);
    }

    @Test
    void getInfo() {
        List<Person> people = sut.getInfo();
        assertThat(people).isNotNull();
        assertThat(people).hasSize(2);
        Set<String> uniqueCodes = people.stream().map(Person::getAddress).map(Address::getZipCode).collect(Collectors.toSet());

    }

    @Test
    void checkDetails() {
        Person existingPerson = findOnePerson();
        Integer existingPersonId = existingPerson.getId();
        Optional<Person> foundPerson = sut.checkDetails(existingPersonId);
        Assertions.assertThat(foundPerson).isPresent();
        Person person = foundPerson.get();
        Address address = person.getAddress();
        assertThat(person.getName()).isEqualTo("Andrew");
        assertThat(person.getSurname()).isEqualTo("Berty");
        assertThat(address).isNotNull();
        assertThat(address.getStreet()).isEqualTo("Elliott Street");
        assertThat(address.getZipCode()).isEqualTo("PL");
        assertThat(address.getCity()).isEqualTo("Warsaw");
        assertThat(address.getHomeNumber()).isEqualTo(32);

    }

    @Test
    void add() {
        Address address = new Address("city", "zipCode", "street", 2);
        Person person = new Person(null, "name", "surname", 34, 160, 60.0d, address);

        int createdId = sut.add(person);

        Optional<Person> foundPerson = sut.checkDetails(createdId);
        Assertions.assertThat(foundPerson).isPresent();

        Person createdPerson = foundPerson.get();
        Address createdAddress = createdPerson.getAddress();

        assertThat(createdPerson.getName()).isEqualTo("name");
        assertThat(createdPerson.getSurname()).isEqualTo("surname");
        assertThat(createdPerson.getAge()).isEqualTo(34);
        assertThat(createdPerson.getHeight()).isEqualTo(160);
        assertThat(createdPerson.getWeight()).isEqualTo(60.0d);
        assertThat(createdAddress).isNotNull();
        assertThat(createdAddress.getHomeNumber()).isEqualTo(2);
        assertThat(createdAddress.getCity()).isEqualTo("city");
        assertThat(createdAddress.getZipCode()).isEqualTo("zipCode");
        assertThat(createdAddress.getStreet()).isEqualTo("street");

    }

    @Test
    void edit() {
        Person existingPerson = findOnePerson();
        existingPerson.setName("UpdatedName");
        existingPerson.setSurname("UpdatedSurname");
        existingPerson.getAddress().setStreet("NewStreet");

        int updateId = sut.edit(existingPerson);

        Optional<Person> foundPerson = sut.checkDetails(updateId);
        Assertions.assertThat(foundPerson).isPresent();
        Person updatedPerson = foundPerson.get();
        assertThat(updatedPerson.getName()).isEqualTo("UpdatedName");
        assertThat(updatedPerson.getSurname()).isEqualTo("UpdatedSurname");
        assertThat(updatedPerson.getAddress().getStreet()).isEqualTo("NewStreet");


    }

    @Test
    void remove() {
        Person existingPerson = findOnePerson();
        Integer existingPersonId = existingPerson.getId();

        sut.remove(existingPersonId);
        Optional<Person> removedPerson = sut.checkDetails(existingPersonId);
        Assertions.assertThat(removedPerson).isNotPresent();
    }

    @Test
    void queryByNameOrSurname() {
        List<Person> personName = sut.queryByNameOrSurname("Andrew");
        Assertions.assertThat(personName).hasSize(1);
        assertThat(personName.get(0).getName()).isEqualTo("Andrew");
        assertThat(personName.get(0).getSurname()).isEqualTo("Berty");

        List<Person> personSurname = sut.queryByNameOrSurname("Berty");
        Assertions.assertThat(personSurname).hasSize(1);
        assertThat(personSurname.get(0).getSurname()).isEqualTo("Berty");
        assertThat(personSurname.get(0).getName()).isEqualTo("Andrew");

    }

    @Test
    void sortedByGivenCriteria() {
        List<Person> peopleSortedByName = sut.sortByGivenCriteria("name");
        assertThat(peopleSortedByName).hasSize(2);
        assertThat(peopleSortedByName).isNotNull();
        assertThat(peopleSortedByName).containsExactly(peopleSortedByName.get(0), peopleSortedByName.get(1));

        List<Person> peopleSortedBySurname = sut.sortByGivenCriteria("surname");
        assertThat(peopleSortedBySurname).isNotNull();
        assertThat(peopleSortedBySurname).hasSize(2);
        assertThat(peopleSortedBySurname).containsExactly(peopleSortedBySurname.get(0), peopleSortedBySurname.get(1));

        List<Person> peopleSortedByAge = sut.sortByGivenCriteria("age");
        assertThat(peopleSortedByAge).isNotNull();
        assertThat(peopleSortedByAge).hasSize(2);
        assertThat(peopleSortedByAge).containsExactly(peopleSortedByAge.get(0), peopleSortedByAge.get(1));


        List<Person> peopleSortedByCity = sut.sortByGivenCriteria("city");
        assertThat(peopleSortedByCity).hasSize(2);
        assertThat(peopleSortedByCity).isNotNull();
        assertThat(peopleSortedByCity).containsExactly(peopleSortedByCity.get(0), peopleSortedByCity.get(1));


        List<Person> peopleSortedByStreet = sut.sortByGivenCriteria("street");
        assertThat(peopleSortedByStreet).hasSize(2);
        assertThat(peopleSortedByStreet).isNotNull();
        assertThat(peopleSortedByStreet).containsExactly(peopleSortedByStreet.get(0), peopleSortedByStreet.get(1));

    }

    private Person findOnePerson() {
        final Optional<Person> existingPerson = sut.getInfo()
                .stream()
                .filter(person -> person.getName().equals("Andrew"))
                .findFirst();

        Assertions.assertThat(existingPerson).isPresent();
        return existingPerson.get();
    }
}