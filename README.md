# People Data Base Application
Simple self made data base application enables the user to:

* list basic people info
* check details of given person
* add new person
* edit person
* delete person
* edit person's address data
* looking for a person by name or surname
* list basic people info sorted by given criteria (name/surname/age/city/street)

## Application connected with Data base using Hibernate.

### Technologies:
* Hibernate Core 5.4. Final
* MySQL Connector 8.0.
* SLF4J API 1.7.
* Logback Core 1.2.3.
* Project Lombok 1.18
* JUnit Jupiter API 5.1.
* Apache Commons lang 3.8.1.